# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'

import os, sys
sys.path.append(os.path.join("..", "..", "main", "python"))

import unittest
import numpy as np
import pandas as pd
from mock import patch, Mock, MagicMock
from sql4csv.tables import PandasTable
from sql4csv.expressions import *


class TestTables(unittest.TestCase):

    def test_init(self):
        table = PandasTable("sometablename")

        self.assertEquals("sometablename", table.name)
        self.assertEquals(None, table.data)

    def test_select_star(self):
        data = Mock()
        table = PandasTable("sometablename")
        table.data = data

        select_star = table.select(["*"])

        self.assertEquals(data, select_star, "Incorrect result of select")

    def test_select_column_list(self):
        data = Mock()
        data.loc = MagicMock()
        table = PandasTable("sometablename")
        table.data = data
        columns = ["a", "b", "c"]

        select_columns = table.select(columns)
        data.loc.__getitem__.assert_called_once_with((slice(None, None, None), columns))

    def test_select_columns_aggregated(self):
        table = PandasTable("sometable")
        data = {"a": np.arange(10), "b": np.arange(10, 20), "c": np.arange(1, 11)}
        table.data = data
        columns = [("max", "a"),  ("min", "b"),  ("avg", "c")]

        result = table.select(columns)
        self.assertEquals([9, 10, 5.5], result, "Incorrect aggregation results")

    def test_select_columns_aggregated_unknown_aggregation_raises_exception(self):
        table = PandasTable("sometable")
        columns = [("max", "a"),  ("median", "b")]

        self.assertRaises(ValueError, table.select, columns)

    def test_select_unsupported_columns_raise_exception(self):
        table = PandasTable("sometablename")
        self.assertRaises(ValueError, table.select, 123)

    def test_get_filter_none(self):
        expression = None
        table = PandasTable("sometable")
        filter_ = table._get_filter(expression)

        self.assertIsNone(filter_, "Filter on None expression should be None")

    def test_get_filter_on_non_expression_raises_exception(self):
        expression = "a < 5"
        table = PandasTable("sometable")

        self.assertRaises(NotImplementedError, table._get_filter, expression)

    def test_get_filter_simple_expression(self):
        expression = SimpleExpression("a", "<", "5")
        table = PandasTable("somename")
        filter_pass = [{"a": -1}, {"a": 4}]
        filter_fail = [{"a": 25}, {"a": 5}]

        filter_ = table._get_filter(expression)

        self.assertTrue(all(filter_(pas) for pas in filter_pass), "Filter not returning True on correct data")
        self.assertFalse(any(filter_(fail) for fail in filter_fail), "Filter not returning False on incorrect data")

    def test_get_filter_conjunction(self):
        expression = ConjunctionExpression(
            SimpleExpression("a", "=", "10"),
            SimpleExpression("b", "<", "20")
        )
        table = PandasTable("somename")
        filter_pass = {"a": 10, "b": 15}
        filter_fail = [{"a": 10, "b": 25}, {"a": 12, "b": 15}]

        filter_ = table._get_filter(expression)

        self.assertTrue(filter_(filter_pass), "Filter not returning True on correct data")
        self.assertFalse(any(filter_(fail) for fail in filter_fail), "Filter not returning False on incorrect data")

    def test_get_filter_alternative(self):
        expression = AlternativeExpression(
            SimpleExpression("a", "!=", "10"),
            SimpleExpression("b", ">", "20")
        )

        table = PandasTable("somename")
        filter_pass = [{"a": 9, "b": 21}, {"a": 11, "b": 32}, {"a": 10, "b": 25}]
        filter_fail = [{"a": 10, "b": 15}, {"a": 10, "b": 20}]

        filter_ = table._get_filter(expression)

        self.assertTrue(all(filter_(pas) for pas in filter_pass), "Filter not returning True on correct data")
        self.assertFalse(any(filter_(fail) for fail in filter_fail), "Filter not returning False on incorrect data")

    def test_get_filter_unsupported_operator_raises_exception(self):
        expression = SimpleExpression("a", "->", "5")
        table = PandasTable("somename")

        self.assertRaises(NotImplementedError, table._get_filter, expression)

    @patch.object(PandasTable, '_get_filter')
    def test_filter(self, get_filter):
        table = PandasTable("somename")
        table.data = MagicMock()
        expression = SimpleExpression("a", "<", "5")
        filter_function = lambda x: x["a"] < 5
        get_filter.return_value = filter_function
        table.filter(expression)

        get_filter.assert_called_once_with(expression)
        table.data.__getitem__.assert_called_once_with(filter_function)

    def test_filter_where_is_none(self):
        data = Mock()
        table = PandasTable("somename")
        table.data = data

        filtered = table.filter(None)

        self.assertEquals(data, filtered.data, "Filtering with None should return original data")
