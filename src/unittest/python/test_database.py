# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'

import os, sys
sys.path.append(os.path.join("..", "..", "main", "python"))

import unittest
from mock import Mock, patch
from sql4csv.database import Database, Table


class TestDatabase(unittest.TestCase):

    @patch('os.path.exists')
    def test_nonexistent_path_raises_exception(self, exists):
        exists.return_value = False
        self.assertRaises(ValueError, Database, "/some/non/existent/path/to/database/")

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_path_without_tables_raises_exception(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1", "file2", "file3"]
        self.assertRaises(ValueError, Database, "/some/existing/path/with/no/csv/file/")

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_correct_constructor(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        database = Database("/some/correct/path", separator=";")
        self.assertEquals("/some/correct/path", database.path, "Wrong database path")
        self.assertEquals({}, database.tables, "Wrong database tables initialization")
        self.assertEquals(";", database.separator, "Wrong database separator")

    @patch.object(Table, 'load_data')
    @patch.object(Table, '__init__')
    @patch('os.listdir')
    @patch('os.path.exists')
    def test_load_existing_table(self, exists, listdir, table_init, table_load):
        db_path = "/some/correct/path"
        table_name = "sometable"
        table_path = os.path.join(db_path, table_name + ".csv")
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        table_init.return_value = None

        database = Database(db_path, separator=";")
        database.load_table(table_name)

        table_init.assert_called_once_with(table_path)
        table_load.assert_called_once_with(table_path, separator=";")
        self.assertIsNotNone(database.tables[table_name], "Table not loaded in database")
        self.assertIsInstance(database.tables[table_name], Table, "Wrong table type in database")

    @patch.object(Table, 'load_data')
    @patch.object(Table, '__init__')
    @patch('os.listdir')
    @patch('os.path.exists')
    def test_load_nonexisting_table_raises_exception(self, exists, listdir, table_init, table_load):
        db_path = "/some/correct/path"
        table_name = "sometable"
        table_path = os.path.join(db_path, table_name + ".csv")
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        table_init.return_value = None

        database = Database(db_path, separator=";")
        exists.return_value = False

        self.assertRaises(ValueError, database.load_table, table_name)

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_get_cached_table(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        mock_table = Mock()
        database = Database("/some/correct/path", separator=";")
        database.tables = {"sometable": mock_table}
        table = database.table("sometable")

        self.assertEquals(mock_table, table, "Wrong table retreived")

    @patch.object(Database, 'load_table')
    @patch('os.listdir')
    @patch('os.path.exists')
    def test_get_noncached_table_rethrowing_error(self, exists, listdir, load_table):
        load_table.side_effect = KeyError("Error occurred")
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        database = Database("/some/correct/path", separator=";")

        self.assertRaises(ValueError, database.table, "somename")

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_query_filter_exception_raise_exception(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        mock_table = Mock()
        mock_table.filter.side_effect = Exception("Some exception during filtering")
        database = Database("/some/correct/path", separator=";")
        database.table = Mock()
        database.table.return_value = mock_table

        self.assertRaises(ValueError, database.query, "tablename", ["a", "b", "c"], None)

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_query_select_exception_raise_exception(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        mock_table = Mock()
        mock_table.filter.return_value = mock_table
        mock_table.select.side_effect = Exception("Some exception during filtering")
        database = Database("/some/correct/path", separator=";")
        database.table = Mock()
        database.table.return_value = mock_table

        self.assertRaises(ValueError, database.query, "tablename", ["a", "b", "c"], None)

    @patch('os.listdir')
    @patch('os.path.exists')
    def test_query(self, exists, listdir):
        exists.return_value = True
        listdir.return_value = ["file1.csv", "file2.csv", "file3.csv"]
        mock_data = Mock()
        mock_table = Mock()
        mock_table.filter.return_value = mock_table
        mock_table.select.return_value = mock_data
        database = Database("/some/correct/path", separator=";")
        database.table = Mock()
        database.table.return_value = mock_table

        result = database.query("tablename", ["a", "b", "c"], None)

        self.assertEquals(mock_data, result, "Wrong result of the query")
