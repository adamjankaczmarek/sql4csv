# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'

import os, sys
sys.path.append(os.path.join("..", "..", "main", "python"))

import unittest
from sql4csv.parser import parse_query
from sql4csv.expressions import *


class TestParserModule(unittest.TestCase):

    def test_incorrect_query_raises_exception(self):
        query = "from table select x,y,z where a<3"
        self.assertRaises(ValueError, parse_query, query)

    def test_agregated_and_not_aggregated_columns_raise_exception(self):
        query = "SELECT max(a), b FROM table"
        self.assertRaises(ValueError, parse_query, query)

    def test_wrong_where_clause_raises_exception(self):
        query = "SELECT a, b FROM table WHERE x <> y"
        self.assertRaises(ValueError, parse_query, query)

    def test_lower_case_query(self):
        query = "select x,y,z from table"
        expected_parse = ("table", ["x", "y", "z"], None)
        result_parse = parse_query(query)
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s" % query)

    def test_mixed_case_query(self):
        query = "SeLeCt x,y,z fRom table"
        expected_parse = ("table", ["x", "y", "z"], None)
        result_parse = parse_query(query)
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s" % query)

    def test_simple_query(self):
        query = "SELECT x,y,z FROM table"
        expected_parse = ("table", ["x", "y", "z"], None)
        result_parse = parse_query(query)
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s" % query)

    def test_query_with_where_clause(self):
        query = "SELECT x,y,z FROM table WHERE a < 20"
        expected_condition_string = "a < 20"
        expected_parse = ("table", ["x", "y", "z"], SimpleExpression("a", "<", "20"))
        result_parse = parse_query(query)
        self.assertEquals(expected_condition_string, str(result_parse[2]).lower())
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s: " % query)

    def test_query_with_complex_where_clause(self):
        query = "SELECT x,y,z FROM table WHERE a < 20 AND b = abc OR c > 10 AND b != cba"
        expected_condition_string = "a < 20 and b = abc or c > 10 and b != cba"
        expected_parse = ("table", ["x", "y", "z"], AlternativeExpression(
            ConjunctionExpression(
                SimpleExpression("a", "<", "20"),
                SimpleExpression("b", "=", "abc")
            ),
            ConjunctionExpression(
                SimpleExpression("c", ">", "10"),
                SimpleExpression("b", "!=", "cba")
            )
        ))
        result_parse = parse_query(query)
        self.assertEquals(expected_condition_string, str(result_parse[2]).lower())
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s: " % query)

    def test_aggregated_query(self):
        query = "SELECT max(x), min(y) FROM table"
        expected_parse = ("table", [("max", "x"), ("min", "y")], None)
        result_parse = parse_query(query)
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s" % query)

    def test_aggregated_query_with_where(self):
        query = "SELECT max(x), min(y), avg(z) FROM table WHERE a > 12"
        expected_condition_string = "a > 12"
        expected_parse = ("table", [("max", "x"), ("min", "y"), ("avg", "z")], SimpleExpression("a", ">", "12"))
        result_parse = parse_query(query)
        self.assertEquals(expected_condition_string, str(result_parse[2]).lower())
        self.assertEquals(expected_parse, result_parse, "Wrong parsing result for query %s" % query)
