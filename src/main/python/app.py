# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'
import sys
from sql4csv.database import Database
from sql4csv.parser import parse_query


def respond(db, query):
    return db.query(*parse_query(query))


def run_app(db_path):
    db = Database(db_path)
    while True:
        try:
            query = raw_input("sql4csv >> ")
            answer = respond(db, query)
            print answer
        except ValueError as e:
            print e.message
        except KeyboardInterrupt:
            break
        except EOFError:
            break


if __name__ == "__main__":
    run_app(sys.argv[1])
