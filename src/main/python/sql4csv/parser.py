# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'
import re
from expressions import *

_QUERY = r"^SELECT (.*?) FROM (.*?)( WHERE (.*?))?$"
_AGG_COLUMN = r"^(.*?)\((.*?)\)$"
_OR_EXPRESSION = r"^(.*)? OR (.*)?$"
_AND_EXPRESSION = r"^(.*?) AND (.*)?$"
_SIMPLE_CONDITION = r"^(.*)? (>|<|=|!=) (.*)?$"


def parse_query(query):
    """
    Function for parsing sql4csv queries into three parts:
    - table_name - name of the table to select data from
    - columns - columns to be selected
    - conditions - filter conditions from WHERE clause
    :param query: query string to be parsed
    :return: triple (table_name, columns, conditions)
    """
    query_match = re.match(_QUERY, query, re.IGNORECASE)
    if query_match is not None:
        return query_match.group(2), parse_columns(query_match.group(1)), parse_condition(query_match.group(4))
    else:
        raise ValueError("Wrong SQL query provided: %s" % query)


def parse_columns(columns):
    """
    Parses column list into either list in case of simple columns
    or into dict of column : aggregation entries in case of aggregated columns
    Accepts only one type of columns at once - either all columns are simple or
    all columns are aggregated
    :param columns: raw columns description from query
    :return: list of simple columns or dict of column : aggregation entries
    """
    columns_split = columns.split(",")
    columns_agg_match = [re.match(_AGG_COLUMN, column.strip(), re.IGNORECASE) for column in columns_split]
    if None not in columns_agg_match:
        return [(m.group(1), m.group(2)) for m in columns_agg_match]
    elif all(match is None for match in columns_agg_match):
        return [column.strip() for column in columns_split]
    else:
        raise ValueError("Columns should be either all aggregated or all not aggregated")


def parse_condition(conditions):
    """
    Parses where condition into corresponding Expression object
    :param conditions: where condition phrase
    :return: Expression corresponding go given where condition
    """
    if conditions is None:
        return None

    or_match = re.match(_OR_EXPRESSION, conditions, re.IGNORECASE)
    and_match = re.match(_AND_EXPRESSION, conditions, re.IGNORECASE)
    simple_match = re.match(_SIMPLE_CONDITION, conditions, re.IGNORECASE)

    if or_match:
        return AlternativeExpression(
            parse_condition(or_match.group(1)),
            parse_condition(or_match.group(2))
        )
    elif and_match:
        return ConjunctionExpression(
            parse_condition(and_match.group(1)),
            parse_condition(and_match.group(2))
        )
    elif simple_match:
        return SimpleExpression(
            simple_match.group(1),
            simple_match.group(2),
            simple_match.group(3)
        )
    else:
        raise ValueError("Unsupported WHERE clause: %s" % conditions)
