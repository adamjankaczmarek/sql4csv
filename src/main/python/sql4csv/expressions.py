# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'


class SimpleExpression(object):
    """
    SimpleExpression object - corresponds to a single condition
    expression from where clause eg. a < 10
    """

    def __init__(self, variable, operator, value):
        self._variable = variable
        self._operator = operator
        self._value = value

    @property
    def variable(self):
        return self._variable

    @property
    def operator(self):
        return self._operator

    @property
    def value(self):
        try:
            return float(self._value)
        except ValueError:
            return self._value

    def __eq__(self, other):
        return self.variable == other.variable and self.operator == other.operator and self.value == other.value

    def __str__(self):
        return " ".join([self._variable, self._operator, self._value])


class DoubleExpression(object):
    """
    DoubleExpression object - corresponding to a condition from where clause
    consisting of two subordinate expressions
    """

    def __init__(self, left_expression, right_expression):
        self._left = left_expression
        self._right = right_expression

    @property
    def left(self):
        return self._left

    @property
    def right(self):
        return self._right

    def __eq__(self, other):
        return self.left == other.left and self.right == other.right


class AlternativeExpression(DoubleExpression):
    """
    AlternativeExpression object - corresponding to a condition from where clause
    constituted by OR logical operator
    """

    def __init__(self, left_expression, right_expression):
        super(AlternativeExpression, self).__init__(left_expression, right_expression)

    def __str__(self):
        return " ".join([str(self._left), "OR", str(self._right)])


class ConjunctionExpression(DoubleExpression):
    """
    ConjunctionExpression object - corresponding to a condition from where clause
    constituted by AND logical operator
    """

    def __init__(self, left_expression, right_expression):
        super(ConjunctionExpression, self).__init__(left_expression, right_expression)

    def __str__(self):
        return " ".join([str(self._left), "AND", str(self._right)])
