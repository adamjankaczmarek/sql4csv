# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'
import pandas as pd
from enum import Enum
from expressions import *


class Aggregation(Enum):
    """
    Enumeration of aggregation functions with their implementations
    available to PandasTable
    """
    MAX = (lambda column: column.max(), )
    MIN = (lambda column: column.min(), )
    AVG = MEAN = (lambda column: column.mean(), )

    def __init__(self, fun):
        self.fun = fun

    def apply(self, column):
        return self.fun(column)


class PandasTable(object):
    """
    Table representation object based on Pandas DataFrame
    """

    def __init__(self, name):
        self._name = name
        self._data = None

    @property
    def name(self):
        return self._name

    @property
    def data(self):
        return self._data

    @data.setter
    def data(self, data):
        self._data = data

    def load_data(self, path, separator=","):
        """
        Reads data from csv file into table's data field
        :param path: path to the csv file
        :param separator: separator used in csv file
        """
        self.data = pd.read_csv(path, sep=separator)

    def select(self, columns):
        """
        Performs selection or aggregation of table's columns
        :param columns: columns to be selected - list of column names or list of pairs (aggregation, column name)
        :return: subset of table's data defined by provided columns
        """
        if isinstance(columns, list):
            if "*" in columns:
                return self.data
            elif isinstance(columns[0], tuple):
                try:
                    return [Aggregation[agg.upper()].apply(self.data[column]) for agg, column in columns]
                except (TypeError, KeyError):
                    raise ValueError("Unsupported aggregation provided: %s" % columns)
            else:
                return self.data.loc[:, columns].values
        else:
            raise ValueError("Unsupported column format supplied")

    def filter(self, where):
        """
        Filters data according to given Expression and returns new Table
        consisting only of the data that meet condition described by the Expression
        :param where: Expression of the where clause condition
        :return: new table with filtered data
        """
        new_table = PandasTable(self._name)
        new_table.data = self.data[self._get_filter(where)] if where is not None else self.data
        return new_table

    def _get_filter(self, expression):
        """
        PandasTable-specific conversion method from Expression to internally applicable filter function
        :param expression: Expression of the where condition from parser
        :return: Internally applicable filter function for the PandasTable
        """
        if expression is None:
            return None

        if isinstance(expression, AlternativeExpression):
            left_filter = self._get_filter(expression.left)
            right_filter = self._get_filter(expression.right)
            return lambda x: (left_filter(x)) | (right_filter(x))
        elif isinstance(expression, ConjunctionExpression):
            left_filter = self._get_filter(expression.left)
            right_filter = self._get_filter(expression.right)
            return lambda x: (left_filter(x)) & (right_filter(x))
        elif isinstance(expression, SimpleExpression):
            if "=" == expression.operator:
                return lambda x: x[expression.variable] == expression.value
            elif "!=" == expression.operator:
                return lambda x: x[expression.variable] != expression.value
            elif "<" == expression.operator:
                return lambda x: x[expression.variable] < expression.value
            elif ">" == expression.operator:
                return lambda x: x[expression.variable] > expression.value
            else:
                raise NotImplementedError("Cannot convert %s to pandas filter" % type(expression))
        else:
            raise NotImplementedError("Cannot convert %s to pandas filter" % type(expression))
