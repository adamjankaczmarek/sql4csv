# -*- coding: utf-8 -*-
__author__ = 'Adam Kaczmarek <adamjankaczmarek@gmail.com>'
import os
from tables import PandasTable as Table


class Database(object):
    """
    Database object - interface for executing queries on tables
    Responsible triggering loading table data and for general query execution flow
    """

    def __init__(self, db_path, separator=","):
        """
        Initialization of database with verification of existence of the provided database directory
        and at least one table (csv file) in that directory
        :param db_path: The path to database's directory
        :param separator: The separator used in csv files storing table data
        :return:
        """

        if not os.path.exists(db_path):
            raise ValueError("Database path: %s does not exist" % db_path)

        if len([f for f in os.listdir(db_path) if f.endswith("csv")]) <= 0:
            raise ValueError("Database in path: %s does not contain any tables" % db_path)

        self.path = db_path
        self.separator = separator
        self.tables = {}

    def load_table(self, table_name):
        """
        For given table name creates table and triggers data loading process
        Table is stored in internal table dictionary indexed by table names
        :param table_name: name of the table
        :return:
        """
        table_path = os.path.join(self.path, table_name + ".csv")
        if not os.path.exists(table_path):
            raise ValueError("No such table in database: %s" % table_name)
        table = Table(table_path)
        table.load_data(table_path, separator=self.separator)
        self.tables[table_name] = table

    def table(self, table_name):
        """
        Retreives the table structure for given name.
        If table does not exists in the table space dictionary, calls self.load_table method
        to create the table and load its data
        :param table_name: name of the table to be retreived
        :return: Table object corresponding to provided name
        """
        if table_name not in self.tables:
            try:
                self.load_table(table_name)
            except Exception as e:
                raise ValueError("No such table in database: %s" % table_name)

        return self.tables[table_name]

    def query(self, table_name, columns, where):
        """
        Performs query on table in following schema:
        - apply where condition filter on the table
        - select specified columns
        :param table_name: name of the table to execute query on
        :param columns: columns to be selected from the resulting (filtered) table
        :param where: Expression describing the where clause condition
        :return: Data from specified table according to the query
        (specified columns from rows satisfying where condition)
        """
        table = self.table(table_name)
        try:
            filtered = table.filter(where)
            return filtered.select(columns)
        except Exception:
            raise ValueError("Error during query execution")
